\babel@toc {ngerman}{}
\babel@toc {ngerman}{}
\contentsline {part}{\numberline {1}Überblick über die Welt}{1}%
\contentsline {chapter}{\numberline {1}Gandref}{2}%
\contentsline {paragraph}{Hinweis}{2}%
\contentsline {section}{Die Kontinente}{2}%
\contentsline {subsection}{Lavenia}{2}%
\contentsline {subsubsection}{Karte}{2}%
\contentsline {subsubsection}{Politisches Gefüge}{2}%
\contentsline {subsubsection}{Bevölkerung}{2}%
\contentsline {subsection}{Kaleph}{2}%
\contentsline {chapter}{\numberline {2}Lavenias Ostküste}{3}%
\contentsline {subsection}{Karte}{3}%
\contentsline {subsection}{Städte und Orte}{3}%
\contentsline {subsubsection}{Flusswasser}{3}%
\contentsline {subsubsection}{Leilon}{3}%
\contentsline {subsubsection}{Phandalin}{3}%
\contentsline {part}{\numberline {2}Regelübersichten}{4}%
\contentsline {section}{Kampf}{5}%
\contentsline {subsection}{Bewegung}{5}%
\contentsline {subsection}{Aktion}{5}%
\contentsline {subsection}{Bonusaktion}{5}%
\contentsline {subsubsection}{Mit einem Objekt interagieren}{5}%
\contentsline {subsubsection}{Andere einfache Aktivitäten}{5}%
\contentsline {subsubsection}{evtl. eine Bonusaktion}{5}%
\contentsline {subsubsection}{evtl. eine Reaktion}{5}%
\contentsline {subsection}{Kampfaktionen}{5}%
\contentsline {subsubsection}{Angriff}{5}%
\contentsline {subsubsection}{Ausweichen}{5}%
\contentsline {subsubsection}{Gegenstand benutzen}{5}%
\contentsline {subsubsection}{Helfen}{6}%
\contentsline {subsubsection}{Rückzug}{6}%
\contentsline {subsubsection}{Spurt}{6}%
\contentsline {subsubsection}{Stabilisieren}{6}%
\contentsline {subsubsection}{Suchen}{6}%
\contentsline {subsubsection}{Verstecken}{6}%
\contentsline {subsubsection}{Vorbereiten}{6}%
\contentsline {subsubsection}{Zauber wirken}{6}%
\contentsline {section}{Erkundung und Fertigkeiten}{6}%
\contentsline {subsection}{Stärke}{6}%
\contentsline {subsubsection}{Stärkewürfe}{6}%
\contentsline {paragraph}{Athletik}{6}%
\contentsline {paragraph}{Andere Stärkewürfe}{6}%
\contentsline {paragraph}{Objekte schieben, ziehen od. anheben}{6}%
\contentsline {subsection}{Geschicklichkeit}{6}%
\contentsline {subsubsection}{Geschicklichkeitswürfe}{6}%
\contentsline {paragraph}{Akrobatik}{6}%
\contentsline {paragraph}{Fingerfertigkeit}{6}%
\contentsline {paragraph}{Heimlichkeit}{7}%
\contentsline {paragraph}{Andere Ges.-Würfe}{7}%
\contentsline {subsection}{Konstitution}{7}%
\contentsline {subsubsection}{Konstitutionswürfe}{7}%
\contentsline {subsection}{Intelligenz}{7}%
\contentsline {subsubsection}{Intelligenzwürfe}{7}%
\contentsline {paragraph}{Arkane Kunde}{7}%
\contentsline {paragraph}{Geschichte}{7}%
\contentsline {paragraph}{Nachforschungen}{7}%
\contentsline {paragraph}{Naturkunde}{7}%
\contentsline {paragraph}{Religion}{7}%
\contentsline {paragraph}{Andere Int.-Würfe}{7}%
\contentsline {subsection}{Weisheit}{7}%
\contentsline {subsubsection}{Weisheitswürfe}{7}%
\contentsline {paragraph}{Mit Tieren umgehen}{7}%
\contentsline {paragraph}{Motiv erkennen}{7}%
\contentsline {paragraph}{Heilkunde}{7}%
\contentsline {paragraph}{Wahrnehmung}{7}%
\contentsline {paragraph}{Überlebenskunst}{8}%
\contentsline {paragraph}{Andere Weisheitswürfe}{8}%
\contentsline {subsection}{Charisma}{8}%
\contentsline {subsubsection}{Charismawürfe}{8}%
\contentsline {paragraph}{Täuschen}{8}%
\contentsline {paragraph}{Einschüchtern}{8}%
\contentsline {paragraph}{Auftreten}{8}%
\contentsline {paragraph}{Überzeugen}{8}%
\contentsline {paragraph}{Andere Charismawürfe}{8}%
\contentsline {part}{\numberline {3}Übersicht über Zauber und Ausrüstung}{9}%
\contentsline {chapter}{\numberline {3}Abenteuerausrüstung}{10}%
\contentsline {chapter}{\numberline {4}Baleor (Titus)}{11}%
\contentsline {section}{Zauber}{11}%
\contentsline {subsection}{Zaubertricks}{11}%
\contentsline {subsubsection}{Licht}{11}%
\contentsline {subsubsection}{Magierhand}{11}%
\contentsline {subsubsection}{Kältestrahl}{11}%
\contentsline {subsection}{Zaubersprüche}{11}%
\contentsline {subsubsection}{Brennende Hände}{11}%
\contentsline {subsubsection}{Federfall}{11}%
\contentsline {subsubsection}{Magierrüstung}{11}%
\contentsline {subsubsection}{Magisches Geschoss}{12}%
\contentsline {subsubsection}{Schlaf}{12}%
\contentsline {subsubsection}{Person Bezaubern}{12}%
\contentsline {chapter}{\numberline {5}Lureene (Maria)}{13}%
\contentsline {section}{Zauber}{13}%
\contentsline {subsection}{Zaubertricks}{13}%
\contentsline {subsubsection}{Heilige Flamme}{13}%
\contentsline {subsubsection}{Verschonung der Toten}{13}%
\contentsline {subsubsection}{Thaumarturgie}{13}%
\contentsline {subsection}{Zaubersprüche}{13}%
\contentsline {subsubsection}{Heiligtum}{13}%
\contentsline {subsubsection}{Lenkendes Geschoss}{13}%
\contentsline {subsubsection}{Segnen}{13}%
\contentsfinish 
\contentsline {subsubsection}{Wunden Heilen}{14}%

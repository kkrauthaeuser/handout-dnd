\documentclass[letterpaper,twocolumn,openany,nodeprecatedcode]{dndbook}

\usepackage[ngerman]{babel}

\usepackage[utf8]{inputenc}
\usepackage[singlelinecheck=false]{caption}
\usepackage{lipsum}
\usepackage{listings}
\usepackage{shortvrb}
\usepackage{stfloats}

\usepackage{graphicx}

\captionsetup[table]{labelformat=empty,font={sf,sc,bf,},skip=0pt}

\MakeShortVerb{|}

\lstset{%
  basicstyle=\ttfamily,
  language=[LaTeX]{TeX},
  breaklines=true,
}

\title{Handout und Notizen zur laufenden DnD Kampagne}
\author{Konstantin Krauthäuser}
\date{\today}

\begin{document}


\frontmatter

\maketitle

\tableofcontents

\mainmatter%

\chapter{Überblick über die Welt}
\paragraph{Hinweis} Dieses Kapitel wird im Laufe der Zeit erweitert, wenn die Spieler mehr über die Welt erfahren.

\chapter{Übersichten}

\section{Kampf}
\begin{DndSidebar}[float=!b]{Überraschung}
  Wurde eine Kreatur von einem Kampf überrascht, verliert sie ihren Zug in der ersten Kampfrunde. Dies schließt alle Reaktionen ein. Die Kreatur kann also erst in der zweiten Kampfrunde agieren.
\end{DndSidebar}
Jede Kampfrunde repräsentiert 6 Sekunden Zeit in der Spielwelt. Alles, was eine Person realisitisch in 6 Sekunden schaffen kann, kann euer Charakter in einer Runde machen. Du darfst dich Bewegen, eine (Haupt-) Aktion und eine Bonusaktion durchführen.

\subsection{Bewegung}
\begin{itemize}
\item Du musst dich nicht bewegen
\item Die Gesamtdistanz in einem Zug darf die Bewegungsrate nicht überschreiten
\item Du darfst dich sowohl vor als auch nach deiner Aktion bewegen, oder beides
\item Deine Bewegung darf Springen, Klettern, Schwingen, Schwimmen oÄ beinhalten, solange dein Charakter dies kann
\end{itemize}

\subsection{Aktion}
\begin{itemize}
\item Du musst keine Aktion durchführen
\item Du kannst alles versuchen, was in weniger als 6 Sekunden Spielzeit machbar ist
\item Am üblichsten sind Angriffsaktionen (siehe unten), individuelle Ideen können dem DM vorgeschlagen werden.
\end{itemize}

\subsection{Bonusaktion}
Neben der Hauptaktion darf \emph{eine} Bonusaktion durchgeführt werden, oft findet dies impliziet statt und muss nicht extra erwähnt werden. Einige Beispiele:

\subsubsection{Mit einem Objekt interagieren}
Du kannst Objekte manipulieren, solange dies nicht komplex ist, bspw:
\begin{itemize}
\item Eine Waffe ziehen/wegstecken
\item Einen Gegenstand von einer Hand in die andere bewegen
\item Eine Armbrust laden
\item Ein Objekt aus einer Tasche holen/in die Tasche stecken
\item ein kleines Objekt aufheben
\item ein kleines Objekt bewegen
\item Eine Kiste öffnen
\item Eine Tür öffnen
\end{itemize}
Mehrere solcher Interaktionen verbrauchen eine ganze Aktion, eine einzelne nur die Bonusaktion.

\subsubsection{Andere einfache Aktivitäten}
Unter Vorbehalt des DMs können andere einfache Aktivitäten durchgeführt werden, wenn sie sich nicht auf die Bewegung auswirken, bspw:
\begin{itemize}
\item mehrere Pfeile aus dem Köcher ziehen
\item Ein Objekt werfen
\item Sich in eine liegende Position fallen lassen (Das Wiederaufstehen verbraucht jedoch 50\% der Bewegung eines Zugs!)
\item Sprechen
\end{itemize}

\subsubsection{evtl. eine Bonusaktion}
Spezielle Fähigkeiten, Zauber uÄ. können, wenn angegeben, als Bonusaktion gewirkt werden. Werden zwei (leichte!) Waffen zweihändig geführt, zählt der Angriff mit der zweiten Waffe als Bonusaktion.

\subsubsection{evtl. eine Reaktion}
Spezielle Fähigkeiten, Zauber uÄ können unter Umständen als Reaktion auf bestimmte Triggerevente gewirkt werden. Es darf nur eine Reaktion pro Runde gewirkt werden, dies muss nicht während des Zugs des betroffenen Spielers passieren. Reaktionen dürfen auch gewirkt werden, wenn schon eine Bonusaktion stattfand. Beispiele:
\begin{itemize}
\item Auch wenn ein Magier nicht am Zug ist, darf er Federfall wirken, wenn er fällt.
\item Wenn ein Gegner versucht an dir vorbeizulafen / dich anngreift und dann versucht sich wegzubewegen bekommst du als Reaktion einen ``kostenlosen'' Gelegenheitsangriff.
\end{itemize}

\begin{DndComment}{Gelegenheitsangriff}
  Bewegt sich eine Kreatur (K1) aus der Nahkampfreichweite (iA 1,5m) einer anderen Kreatur (K2) heraus, kann K2 einen Gelegenheitsangriff auf K1 durchführen.
\end{DndComment}

\subsection{Angriffsaktionen}
\subsubsection{Angriff}
Du kannst einen Nah- oder Fernkampfangriff durchführen.
\subsubsection{Ausweichen}
Bis zu dem Beginn deines nächsten Zuges ist jeder Angriffswurf gegen dich mit Nachteil, wenn du den Angreifer sehen kannst. Außerdem bist du bei Geschicklichkeitsrettungswürfen im Vorteil. Du verlierst diese Vorzüge, wenn du kampfunfähig wirst.
\subsubsection{Gegenstand benutzen}
Evtl. braucht ein Objekt eine Aktion, um verwendet zu werden, oder du nutzt deine Aktion, um mit meheren Objekten zu interagieren.
\subsubsection{Helfen}
Du kannst einem Verbündeten helfen, eine Kreatur, welche sich 1,5m von dir entfernt befindet, anzugreifen. Der Verbündete hat dann einen Vorteil bei seinem ersten Angriffswurf. Nach Rücksprache mit dem DM kannst du auch andersweitig helfen (der Verbündete hat Vorteil bei einer Fertigkeit)
\subsubsection{Rückzug}
Es können keine Gelegenheitsangriffe auf dich durchgeführt werden.
\subsubsection{Spurt}
Deine Bewegunsrate verdoppelt sich.
\subsubsection{Suchen}
Du konzentrierst dich darauf, die Gegend zu untersuchen. Je nach Situation kann der DM einen Wurf auf Weisheit (Warnehmung) oder Intelligenz (Nachforschungen) fordern.
\subsubsection{Verstecken}
Du versuchst dich zu verstecken, und legst einen Wurf auf Geschicklichkeit (Heimlichkeit) ab. Je nach Situation erhälst du Vorzüge.
\subsubsection{Vorbereiten}
Manchmal willst du eine Aktion erst durchführen, wenn etwas bestimmtes passiert. Statt eine Aktion durchzuführen kannst du sie nur vorbereiten und angeben, unter welchen Umständen sie durchgeführt werden soll. Sollte dieser Umstand bis zu deinem nächsten Zug eintreten, kannst du die vorbereitete Aktion als Reaktion wirken. Zauber können nur vorbereitet werden, wenn sie max. eine Aktion lang sind.
\subsubsection{Zauber wirken}
Abhängig von der Wirkdauer kann für Zauber eine Aktion notwendig sein. Das Herausholen evtl. notwendiger Materialen für den Zauber gehört zum Zauber und verbraucht \emph{keine} Bonusaktion.
\section{Erkundung und Fertigkeiten}
Außerhalb des Kampfs gibt es keine feste Reinfolge, in der die Charaktere und NPCs interagieren. Manche Aktionen erforden bestimmte Fertigkeiten bzw. Fertigkeitswürfe, um erfolgreich zu sein. Der DM kann auch entscheiden, dass eine Aktion auf keinen Fall oder immer gelingt. Hier eine Übersicht über die Fertigkeiten:
\subsection{Stärke}
Stärke misst die körperliche Kraft, das athletische Training und das Maß, in dem du Rohe Gewalt ausüben kannst.
\subsubsection{Stärkewürfe}
\paragraph{Athletik}
Würfe auf Stärke (Athletik) decken schwierige Situationen ab, in die du beim Schwimmen, Klettern oder Springen geraten könntest.
\paragraph{Andere Stärkewürfe} Auch in anderen Situationen können Stärkewürfe nötig sein, bspw:
\begin{itemize}
\item Eine klemmende Tür öffnen
\item Fesseln sprengen
\item Sich an einem Wagen festhalten, während man hinter ihm hergezogen wird
\item Einen Felsbrocken daran hindern, davonzurollen
\end{itemize}
\paragraph{Objekte schieben, ziehen od. anheben}
Du kannst das 30-fache deines Stärkewerts an Gewicht bewegen. Darüber sinkt deine Bewegungsrate auf 1,5m.
\subsection{Geschicklichkeit}
Geschicklichkeit umfasst Beweglichkeit, Reflexe und den Gleichgewichtssinn
\subsubsection{Geschicklichkeitswürfe}
Ein Geschicklichkeitswurf kann immer dann zur Anwendung kommen, wenn man sich heimlich, flink od. geschickt bewegen möchte oder auf schwierigen Untergrund nicht das Gleichgewicht verlieren will.
\paragraph{Akrobatik}
Würfe auf Geschicklichkeit (Akrobatik) decken alle Versuche ab, bei denen du auf den Beinen bleiben willst (schwankendes Schiffsdeck, Seil balancieren, Rennen auf Eis usw. ) oder um zu bestimmen, ob dir ein Kunststück gelingt.
\paragraph{Fingerfertigkeit}
Fingerfertigkeit wird verwendet, wenn du Fingerspitzengefühl oder eine schnelle Hand benötigst, bspw. wenn du einen Gegenstand bei dir verstecken willst, oder jmd. einen zustecken willst, ohne dass die Person was merkt.
\paragraph{Heimlichkeit}
Wird bspw. verwendet, wenn du versuchst, dich an Wachen vorbeizuschleichen, dich vor Gegnern versteckst oder versuchst unbemerkt zu entkommen. Auch beim Anschleichen kommt Heimlichkeit zum Einsatz.
\paragraph{Andere Ges.-Würfe}
Auch in anderen Situationen können Geschicklichkeitswürfe vorkommen, bspw:
\begin{itemize}
\item Einen beladenen Wagen auf schwierigen Straßen/Abhängen oÄ unter Kontrolle behalten
\item Ein Schloss knacken
\item Eine Falle entschärfen
\item Einen Gefangenen sicher fässeln
\item Sich aus Fesseln befreien
\end{itemize}
\subsection{Konstitution}
Konstitution misst Ausdauer, Gesundheit und Lebenskraft.
\subsubsection{Konstitutionswürfe}
Konst.-Würfe sind ungewöhnlich, meist wirkt Konstitution passiv. Ein Konst.-Wurf kann herangezogen werden, um die Fähigkeit, über körperliche Grenzen hinauszugehen, zu messen.
\subsection{Intelligenz}
\subsection{Weisheit}
\subsection{Charisma}
\section{Ausrüstung}
\chapter{Baleor (Titus)}

\section{Zaubersprüche}

\DndSpellHeader%
  {Beautiful Typesetting}
  {4th-level illusion}
  {1 action}
  {5 feet}
  {S, M (ink and parchment, which the spell consumes)}
  {Until dispelled}
You are able to transform a written message of any length into a beautiful scroll. All creatures within range that can see the scroll must make a wisdom saving throw or be charmed by you until the spell ends.

While the creature is charmed by you, they cannot take their eyes off the scroll and cannot willingly move away from the scroll. Also, the targets can make a wisdom saving throw at the end of each of their turns. On a success, they are no longer charmed.

\chapter{Lureene (Maria)}
\section{Kleriker des Lichts}
\subsection{Sanzori}
TODO: Gottheit des Leben
\section{Zauber}
\subsection{Zaubertricks}
\DndSpellHeader%
{Heilige Flamme}
{Zaubertrick der Hervorrufung}
{1 Aktion}
{18 Meter}
{V, G}
{unmittelbar}

Flammengleiches Licht senkt sich auf eine Kreatur herab, die sich in Reichweite befindet und die du sehen kannst. Dem Ziel muss ein Geschicklichkeitsrettungswurf gelingen, sonst erleidet es 1W8 gleißenden Schaden. Bei diesem Rettungswurf ist das Ziel nicht im Vorteil durch Deckung.

\DndSpellHeader%
{Verschonung der Toten}
{Zaubertrick der Nekromantie}
{1 Aktion}
{Berührung}
{V, G}
{unmittelbar}

Du berührst eine lebende Kreatur mit 0 Trefferpunkten. Das Ziel wird stabilisiert. Der Zauber hat keine Auswirkungen auf Untote oder Konstrukte.

\DndSpellHeader%
{Thaumarturgie}
{Zaubertrick der Verwandlung}
{1 Aktion}
{9 Meter}
{V}
{bis zu 1 Minute}

Du manifestierst ein kleines Wunder in Reichweite, ein Zeichen übernatürlicher Macht. Du kannst einen der folgendenen (oder vergleichbare) magischen Effekte erzeugen:
\begin{itemize}
\item Deine Stimme dröhnt dreimal so laut wie normal
\item Du lässt Flammen flackern, heller oder dunkler werden oder die Farbe wechseln.
\item Du erzeugst harmlose Erschütterungen des Bodens
\item Du erschaffst ein kurzes Geräuch, das von einem Punkt deiner Wahl in Reichweite ertönt, wie das Grollen von Donner, den Ruf eines Raben oder ein unheilvolles Flüstern.
\item Du lässt eine nicht verriegelte Tür oder ein Fenster auffliegen/zuschlagen
\item Du änderst für 1 Minute das Aussehen deiner Augen.
\end{itemize}

\subsection{Zaubersprüche}
\DndSpellHeader%
{Heiligtum}
{Bannmagie des 1. Grades}
{1 Bonusaktion}
{9 Meter}
{V, G, M (ein kleiner Silberspiegel oÄ)}
{1 Minute}

Du schützt eine Kreatur in Reichweite vor Angriffen. Bis der
Zauber endet, müssen alle Kreaturen, die das geschützte Ziel
mit einem Angriff oder einem schädigenden Zauber attackieren
wollen, zunächst einen Weisheitsrettungswurf ablegen. Bei einem
Misserfolg muss die Kreatur ein neues Ziel wählen oder der Angriff
oder Zauber ist vergeudet. Der magische Schutz bewahrt das
Ziel nicht vor Flächeneffekten, wie der Explosion eines Feuerballs.
Wenn die geschützte Kreatur angreift oder einen Spruch wirkt,
der eine gegnerische Kreatur betrifft, oder anderweitig Schaden
anrichtet, endet dieser Zauber.
\end{document}